{

      "elm/json" = {
        sha256 = "1g0hafkqf2q633r7ir9wxpb1lnlzskhpsyi0h5bkzj0gl072zfnb";
        version = "1.0.0";
      };

      "elm/html" = {
        sha256 = "1n3gpzmpqqdsldys4ipgyl1zacn0kbpc3g4v3hdpiyfjlgh8bf3k";
        version = "1.0.0";
      };

      "elm/svg" = {
        sha256 = "08x0v8p9wm699jjmsnbq69pxv3jh60j4f6fg7y6hyr7xxj85y390";
        version = "1.0.0";
      };

      "elm/browser" = {
        sha256 = "1apmvyax93nvmagwj00y16zx10kfv640cxpi64xgqbgy7d2wphy4";
        version = "1.0.0";
      };

      "elm/core" = {
        sha256 = "10kr86h4v5h4p0586q406a5wbl8xvr1jyrf6097zp2wb8sv21ylw";
        version = "1.0.0";
      };

      "elm-community/list-extra" = {
        sha256 = "0gn76ljvk8hd776zngvhh7p01hkbsclybzsf9b5prq5iy54q7hji";
        version = "8.0.0";
      };

      "elm/random" = {
        sha256 = "138n2455wdjwa657w6sjq18wx2r0k60ibpc4frhbqr50sncxrfdl";
        version = "1.0.0";
      };

      "elm/time" = {
        sha256 = "0vch7i86vn0x8b850w1p69vplll1bnbkp8s383z7pinyg94cm2z1";
        version = "1.0.0";
      };

      "elm/url" = {
        sha256 = "0av8x5syid40sgpl5vd7pry2rq0q4pga28b4yykn9gd9v12rs3l4";
        version = "1.0.0";
      };

      "elm/virtual-dom" = {
        sha256 = "0hm8g92h7z39km325dlnhk8n00nlyjkqp3r3jppr37k2k13md6aq";
        version = "1.0.0";
      };

      "elm-explorations/test" = {
        sha256 = "028nzg5qpanlnxcd5qmldarrinpczw9592yqq10qc9wcx62pspsf";
        version = "1.1.0";
      };
}
