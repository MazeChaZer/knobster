let pkgs = import ./nixpkgs.nix;
    defaultDerivation = import ./default.nix;
in defaultDerivation.overrideAttrs (oldAttrs: {
    name = "knobster-shell";
    buildInputs =
        oldAttrs.buildInputs ++ (with pkgs; [ elm2nix elmPackages.elm-test ]);
})
