import (builtins.fetchTarball {
  name = "nixos-21.05-276671abd104e83ba7cb0b26f44848489eb0306b";
  url = "https://github.com/nixos/nixpkgs/archive/276671abd104e83ba7cb0b26f44848489eb0306b.tar.gz";
  sha256 = "1ly1aydrgisc66bsj21x2klm7ii7746vkiifx1zgws2l6hg4y2wa";
}) {}
