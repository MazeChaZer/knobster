build/app: build/main-licensed.min.js
build/app: $(shell find static/)
	rm -r build/app/ || true
	mkdir build/app/
	cp build/main-licensed.min.js build/app/main.js
	cp -r static/* build/app/

build/main-licensed.min.js: build/main.min.js
	printf '// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-or-later\n' > $@
	cat $< >> $@
	printf '\n// @license-end' >> $@

build/main.min.js: build/main.js
	uglifyjs "$<" --compress "pure_funcs=[F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9],pure_getters,keep_fargs=false,unsafe_comps,unsafe" \
		| uglifyjs --mangle \
		> "$@"

build/main.js: elm.json
build/main.js: $(shell find src/)
	elm make src/Main.elm --optimize --output=$@

.PHONY: test
test:
	elm-test
