let pkgs = import ./nixpkgs.nix;
in pkgs.stdenv.mkDerivation {
    name = "knobster";
    src = ./.;
    buildInputs = with pkgs; [
        elmPackages.elm
        nodePackages.uglify-js
    ];
    buildPhase = pkgs.elmPackages.fetchElmDeps {
        elmPackages = import ./elm-srcs.nix;
        elmVersion = "0.19.1";
        registryDat = ./registry.dat;
    } + ''
        make
    '';
    installPhase = ''
        mv build/app/ $out
    '';
}
