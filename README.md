# Knobster

This is a little browser game built in [Elm](http://elm-lang.org/) using SVG
graphics and CSS animations. You can play it at
<https://knobster.jonas-schuermann.name/>.

# Rules

When the user clicks on a knob, it is turned by 90°. Every other knob that gets
connected to the turned knob by wire is being marked and inherits the color of
the turned knob. After a short delay, all marked knobs are turned and trigger
the same process recursively, until there are no more marked knobs. Knobs that
have been marked multiple times will only be turned once. Then the user is able
to click again. The game is won if all knobs have the same color.

---

Copyright 2017-2019 Jonas Schürmann

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.
