module Main exposing (main)

import Browser exposing (Document)
import Graphics
import Grid exposing (Grid, Position)
import Html exposing (Attribute, Html, a, div, img, input, label, p, text)
import Html.Attributes exposing (alt, checked, class, href, src, target, type_)
import Html.Events exposing (onCheck, onClick, preventDefaultOn)
import Json.Decode exposing (succeed)
import Knob exposing (Color(..), Knob, Orientation(..))
import Size exposing (Size)
import Process
import Random
import Task



---- MODEL ----


type alias Model =
    { game : Game
    , colorblindMode : Bool
    , logoClicks : Int
    }


type alias Game =
    { knobs : Grid Knob
    , steps : Int
    , state : State
    }


type State
    = Waiting
    | Running PendingAction
    | Won


type alias PendingAction =
    { triggeredPositions : List Position
    , color : Color
    }


type alias Flags =
    { seed : Int
    }


init : Flags -> ( Model, Cmd Msg )
init { seed } =
    ( { game = initGame seed
      , colorblindMode = False
      , logoClicks = 0
      }
    , Cmd.none
    )


generateSeed : Cmd Msg
generateSeed =
    Random.generate SeedReceived (Random.int 0 Random.maxInt)


initGame : Int -> Game
initGame seed =
    { knobs =
        Random.step
            (Grid.generator boardSize Knob.generator)
            (Random.initialSeed seed)
            |> Tuple.first
    , steps = 0
    , state = Waiting
    }


boardSize : Size
boardSize = { width = 12, height = 12 }


frameLength : Int
frameLength =
    200



---- UPDATE ----


type Msg
    = KnobClicked Position
    | ProcessTriggeredKnobs
    | PlayAgainClicked
    | SeedReceived Int
    | ColorblindModeToggled Bool
    | LogoClicked
    | NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        KnobClicked position ->
            let
                { game } =
                    model
            in
            case game.state of
                Waiting ->
                    case Grid.get position game.knobs of
                        Just knob ->
                            let
                                ( newGame, cmd ) =
                                    nextStep
                                        { color = Knob.color knob
                                        , triggeredPositions = [ position ]
                                        }
                                        { game | steps = game.steps + 1 }
                            in
                            ( { model | game = newGame }, cmd )

                        Nothing ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        ProcessTriggeredKnobs ->
            let
                { game } =
                    model
            in
            case game.state of
                Running pendingAction ->
                    let
                        ( newGame, cmd ) =
                            nextStep pendingAction game
                    in
                    ( { model | game = newGame }, cmd )

                _ ->
                    ( model, Cmd.none )

        PlayAgainClicked ->
            ( model, Cmd.batch [ generateSeed ] )

        SeedReceived seed ->
            ( { model | game = initGame seed }, Cmd.none )

        ColorblindModeToggled active ->
            ( { model | colorblindMode = active }, Cmd.none )

        LogoClicked ->
            ( { model | logoClicks = model.logoClicks + 1 }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )


nextStep : PendingAction -> Game -> ( Game, Cmd Msg )
nextStep { color, triggeredPositions } model =
    let
        ( newKnobs, newTriggeredPositions ) =
            turnKnobs color triggeredPositions model.knobs

        ( newState, cmd ) =
            if Grid.allEqualBy Knob.color newKnobs then
                ( Won, Cmd.none )

            else
                case newTriggeredPositions of
                    [] ->
                        ( Waiting, Cmd.none )

                    _ ->
                        ( Running
                            { color = color
                            , triggeredPositions = newTriggeredPositions
                            }
                        , Task.perform
                            (always ProcessTriggeredKnobs)
                            (Process.sleep (toFloat frameLength))
                        )
    in
    ( { model
        | knobs = newKnobs
        , state = newState
      }
    , cmd
    )


turnKnobs : Color -> List Position -> Grid Knob -> ( Grid Knob, List Position )
turnKnobs color knobs grid =
    let
        ( newGrid, triggerGrid ) =
            List.foldl
                (\position ( previousGrid, previousTriggerGrid ) ->
                    case Grid.get position previousGrid of
                        Just knob ->
                            let
                                turnedKnob =
                                    Knob.turn color knob
                            in
                            ( Grid.set position turnedKnob previousGrid
                            , Grid.neighbors position previousGrid
                                |> List.filter (Knob.triggeredBy turnedKnob)
                                |> List.map (Tuple.second >> Tuple.first)
                                |> List.foldl
                                    (\innerPosition innerPreviousTriggerGrid ->
                                        Grid.set
                                            innerPosition
                                            True
                                            innerPreviousTriggerGrid
                                    )
                                    previousTriggerGrid
                            )

                        Nothing ->
                            ( previousGrid, previousTriggerGrid )
                )
                ( grid, Grid.repeat boardSize False )
                knobs

        triggeredPositions =
            Grid.toIndexedList triggerGrid
                |> List.filter Tuple.second
                |> List.map Tuple.first
    in
    ( newGrid, triggeredPositions )



---- VIEW ----


view : Model -> Document Msg
view { game, colorblindMode, logoClicks } =
    { title = "Knobster"
    , body =
        [ div
            [ class "container" ]
            [ titleView "logo.svg" logoClicks
            , text "Make all knobs the same color."
            , knobGrid colorblindMode game.knobs
            , progressView colorblindMode game.steps
            , footerView
            , wonView game.state game.steps "lobster.svg"
            ]
        ]
    }


titleView : String -> Int -> Html Msg
titleView logoPath logoClicks =
    let
        logo =
            img
                [ class "logo__image"
                , onClick LogoClicked
                , src logoPath
                , alt "Knobster"
                ]
                []
    in
    div [ class "logo" ]
        [ if logoClicks >= 12 then
            a
                [ href "https://www.youtube.com/watch?v=glUEcQPkiO0"
                , target "blank_"
                ]
                [ logo ]

          else
            logo
        ]


knobGrid : Bool -> Grid Knob -> Html Msg
knobGrid colorblindMode knobs =
    div
        [ class "game-grid" ]
        (List.indexedMap
            (\x column ->
                div
                    [ class "game-grid__column" ]
                    (List.indexedMap
                        (\y knob ->
                            knobView
                                colorblindMode
                                { x = x, y = y }
                                knob
                        )
                        column
                    )
            )
            knobs
        )


knobView : Bool -> Position -> Knob -> Html Msg
knobView colorblindMode position knob =
    div
        [ class "knob"
        , onClick (KnobClicked position)
        , onSelectStartPreventDefault NoOp
        ]
        [ Graphics.knobView frameLength colorblindMode knob ]


progressView : Bool -> Int -> Html Msg
progressView colorblindMode steps =
    div
        [ class "progress" ]
        [ text ("Steps: " ++ String.fromInt steps)
        , label
            [ class "checkbox" ]
            [ input
                [ class "checkbox__input"
                , type_ "checkbox"
                , onCheck ColorblindModeToggled
                , checked colorblindMode
                ]
                []
            , text "Colorblind mode"
            ]
        ]


footerView : Html Msg
footerView =
    div
        [ class "footer" ]
        [ p
            []
            [ text "Inspired by "
            , a
                [ href "http://www.cesmes.fi/flash/boxSpin/", target "blank_" ]
                [ text "boxSpin3" ]
            , text "."
            ]
        , p
            []
            [ text "Developed in "
            , a
                [ href "https://elm-lang.org/", target "blank_" ]
                [ text "Elm" ]
            , text " by "
            , a
                [ href "https://jonas-schuermann.name/", target "blank_" ]
                [ text "Jonas Schürmann" ]
            , text " ("
            , a
                [ href "https://gitlab.com/MazeChaZer/knobster"
                , target "blank_"
                ]
                [ text "Source" ]
            , text ")."
            ]
        , p
            []
            [ text "Artwork by "
            , a
                [ href "https://www.instagram.com/sandrinas_art/", target "blank_" ]
                [ text "SandrinasArt" ]
            ]
        ]


wonView : State -> Int -> String -> Html Msg
wonView state steps lobster =
    case state of
        Won ->
            div
                [ class "won-overlay" ]
                [ div
                    [ class "won-overlay__column" ]
                    [ img
                        [ src lobster
                        , class "lobster"
                        ]
                        []
                    , text
                        ("You win with " ++ String.fromInt steps ++ " steps! ")
                    , a
                        [ class "won-overlay__play-again"
                        , href "#"
                        , onClickPreventDefault PlayAgainClicked
                        ]
                        [ text "Play Again" ]
                    ]
                ]

        _ ->
            text ""


onClickPreventDefault : msg -> Attribute msg
onClickPreventDefault msg =
    preventDefaultOn
        "click"
        (Json.Decode.map alwaysPreventDefault (succeed msg))


onSelectStartPreventDefault : msg -> Attribute msg
onSelectStartPreventDefault msg =
    preventDefaultOn
        "selectstart"
        (Json.Decode.map alwaysPreventDefault (succeed msg))


alwaysPreventDefault : msg -> ( msg, Bool )
alwaysPreventDefault msg =
    ( msg, True )



---- PROGRAM ----


main : Program Flags Model Msg
main =
    Browser.document
        { view = view
        , init = init
        , update = update
        , subscriptions = always Sub.none
        }
