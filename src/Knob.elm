module Knob exposing
    ( Color(..)
    , Knob
    , Orientation(..)
    , angle
    , color
    , faces
    , generator
    , make
    , triggeredBy
    , turn
    )

import Direction exposing (Direction(..))
import Grid exposing (Position)
import Random exposing (Generator)


type Knob
    = Knob
        { color : Color
        , orientation : Orientation
        , rotations : Int
        }


type Color
    = Red
    | Orange
    | Yellow
    | Green
    | Blue


type Orientation
    = NorthWest
    | NorthEast
    | SouthEast
    | SouthWest


make : Color -> Orientation -> Knob
make c orientation =
    Knob
        { color = c
        , orientation = orientation
        , rotations = 0
        }


color : Knob -> Color
color (Knob knob) =
    knob.color


angle : Knob -> Int
angle (Knob { orientation, rotations }) =
    rotations
        * 360
        + (case orientation of
            NorthWest ->
                0

            NorthEast ->
                90

            SouthEast ->
                180

            SouthWest ->
                270
          )


generator : Generator Knob
generator =
    Random.map2 make
        (Random.uniform Red [ Orange, Yellow, Green, Blue ])
        (Random.uniform NorthWest [ NorthEast, SouthEast, SouthWest ])


turn : Color -> Knob -> Knob
turn newColor (Knob knob) =
    let
        newOrientation =
            case knob.orientation of
                NorthWest ->
                    NorthEast

                NorthEast ->
                    SouthEast

                SouthEast ->
                    SouthWest

                SouthWest ->
                    NorthWest

        newRotations =
            knob.rotations
                + (if newOrientation == NorthWest then
                    1

                   else
                    0
                  )
    in
    Knob
        { knob
            | color = newColor
            , orientation = newOrientation
            , rotations = newRotations
        }


faces : Direction -> Knob -> Bool
faces direction (Knob { orientation }) =
    case direction of
        North ->
            orientation == NorthWest || orientation == NorthEast

        East ->
            orientation == NorthEast || orientation == SouthEast

        South ->
            orientation == SouthEast || orientation == SouthWest

        West ->
            orientation == SouthWest || orientation == NorthWest


triggeredBy : Knob -> ( Direction, ( Position, Knob ) ) -> Bool
triggeredBy origin ( direction, ( position, knob ) ) =
    faces direction origin && faces (Direction.opposite direction) knob
