module Size exposing (Size)

type alias Size =
    { width : Int
    , height : Int
    }
