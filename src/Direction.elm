module Direction exposing (Direction(..), opposite)


type Direction
    = North
    | East
    | South
    | West


opposite : Direction -> Direction
opposite direction =
    case direction of
        North ->
            South

        East ->
            West

        South ->
            North

        West ->
            East
