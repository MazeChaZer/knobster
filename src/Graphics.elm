module Graphics exposing (knobView)

import Html exposing (Html)
import Knob exposing (Color(..), Knob)
import Svg exposing (Attribute, Svg, circle, line, path, svg, text)
import Svg.Attributes
    exposing
        ( cx
        , cy
        , d
        , fill
        , height
        , r
        , stroke
        , strokeLinecap
        , strokeWidth
        , width
        , x1
        , x2
        , y1
        , y2
        )


style : List ( String, String ) -> Attribute msg
style properties =
    Svg.Attributes.style <|
        String.join ";" <|
            List.map
                (\( key, value ) -> key ++ ":" ++ value)
                properties


knobView : Int -> Bool -> Knob -> Html msg
knobView frameLength colorblindMode knob =
    svg
        [ width "40"
        , height "40"
        , style
            [ ( "transform", "rotate(" ++ String.fromInt (Knob.angle knob) ++ "deg)" )
            , ( "transition", String.fromInt frameLength ++ "ms" )
            ]
        ]
        [ circle
            [ cx "20"
            , cy "20"
            , r "20"
            , fill <| colorValues <| Knob.color knob
            , style [ ( "cursor", "pointer" ) ]
            ]
            []
        , if colorblindMode then
            arc <| Knob.color knob

          else
            text ""
        , line
            [ x1 "0"
            , y1 "20"
            , x2 "20"
            , y2 "20"
            , stroke "black"
            , strokeWidth "2"
            , strokeLinecap "round"
            , style [ ( "cursor", "pointer" ) ]
            ]
            []
        , line
            [ x1 "20"
            , y1 "0"
            , x2 "20"
            , y2 "20"
            , stroke "black"
            , strokeWidth "2"
            , strokeLinecap "round"
            , style [ ( "cursor", "pointer" ) ]
            ]
            []
        ]


colorValues : Color -> String
colorValues color =
    case color of
        Red ->
            "#F44336"

        Orange ->
            "#FF9800"

        Yellow ->
            "#FFEB3B"

        Green ->
            "#4CAF50"

        Blue ->
            "#3F51B5"


arc : Color -> Svg msg
arc color =
    let
        innerRadius =
            case color of
                Red ->
                    4

                Orange ->
                    3

                Yellow ->
                    2

                Green ->
                    1

                Blue ->
                    0

        ellipseRadius =
            4 * innerRadius + 2

        ellipseMargin =
            (4 - innerRadius) * 4 + 2
    in
    path
        [ d <|
            "M 20 "
                ++ String.fromInt ellipseMargin
                ++ " A "
                ++ String.fromInt ellipseRadius
                ++ " "
                ++ String.fromInt ellipseRadius
                ++ " 0 1 1 "
                ++ String.fromInt ellipseMargin
                ++ ",20"
        , stroke "white"
        , strokeWidth "4"
        , fill "transparent"
        , style [ ( "cursor", "pointer" ) ]
        ]
        []
