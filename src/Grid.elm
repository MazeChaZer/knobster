module Grid exposing
    ( Grid
    , Position
    , allEqualBy
    , generator
    , get
    , neighbors
    , repeat
    , set
    , toIndexedList
    )

import Direction exposing (Direction(..))
import Size exposing (Size)
import List.Extra exposing (getAt, setAt)
import Random exposing (Generator)


type alias Grid a =
    Columns (Cells a)


type alias Columns a =
    List a


type alias Cells a =
    List a


type alias Position =
    { x : Int
    , y : Int
    }


generator : Size -> Generator a -> Generator (Grid a)
generator size g =
    Random.list
        size.width
        (Random.list size.height g)


toIndexedList : Grid a -> List ( Position, a )
toIndexedList grid =
    List.indexedMap
        (\x column ->
            List.indexedMap
                (\y cell ->
                    ( Position x y, cell )
                )
                column
        )
        grid
        |> List.concat


get : Position -> Grid a -> Maybe a
get position grid =
    getAt position.x grid
        |> Maybe.andThen
            (getAt position.y)


set : Position -> a -> Grid a -> Grid a
set position value grid =
    case getAt position.x grid of
        Just column ->
            setAt
                position.x
                (setAt position.y value column)
                grid

        Nothing ->
            grid


neighbors : Position -> Grid a -> List ( Direction, ( Position, a ) )
neighbors origin grid =
    [ ( North, { origin | y = origin.y - 1 } )
    , ( East, { origin | x = origin.x + 1 } )
    , ( South, { origin | y = origin.y + 1 } )
    , ( West, { origin | x = origin.x - 1 } )
    ]
        |> List.filterMap
            (\( direction, position ) ->
                get position grid
                    |> Maybe.map
                        (\value ->
                            ( direction, ( position, value ) )
                        )
            )


allEqualBy : (a -> b) -> Grid a -> Bool
allEqualBy fn grid =
    get { x = 0, y = 0 } grid
        |> Maybe.map
            (\value ->
                let
                    reference =
                        fn value
                in
                List.all
                    (List.all (\x -> fn x == reference))
                    grid
            )
        |> Maybe.withDefault True


repeat : Size -> a -> Grid a
repeat size value =
    List.repeat size.width (List.repeat size.height value)
