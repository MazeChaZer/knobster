module GridTest exposing (suite)

import Expect
import Grid
import Knob exposing (Color(..), Orientation(..))
import Random exposing (initialSeed, step)
import Test exposing (..)


suite : Test
suite =
    describe "Grid"
        [ test "Detect PRNG algorithm changes" <|
            \() ->
                step
                    (Grid.generator
                        { width = 12, height = 12 }
                        Knob.generator
                    )
                    (initialSeed 1337)
                    |> Tuple.first
                    |> List.head
                    |> Expect.equal
                        (Just
                            [ Knob.make Green SouthWest
                            , Knob.make Green NorthEast
                            , Knob.make Orange SouthEast
                            , Knob.make Red SouthEast
                            , Knob.make Red SouthWest
                            , Knob.make Green SouthEast
                            , Knob.make Red NorthEast
                            , Knob.make Red NorthEast
                            , Knob.make Yellow SouthWest
                            , Knob.make Green SouthWest
                            , Knob.make Orange SouthEast
                            , Knob.make Blue NorthWest
                            ]
                        )
        ]
